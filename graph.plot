set terminal png notransparent nocrop enhanced  font "arial,8"
set output 'ping.png'
set key inside left top vertical Right noreverse enhanced \
    autotitle box lt black linewidth 1.000 dashtype solid
set title "ping times"
set title  font ",20" norotate

# Output from mysql is normaly formated as ascii-boxes, with the flag
# -B it is just tab-separated.
set datafile separator "\t"

set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%H:%M:%S"
set xtics rotate by -30

set yrange [0:*]

# gnuplot asking mysql for the values to plot
plot '< mysql -u root -B pingDB -e "SELECT Zeitpunkt, Ping FROM pingtimes;"' \
     using 1:2 title 'www.schauderbasis.de' with lines lw 2 lc "dark-red" smooth csplines


