database_name = pingDB
table_name = pingtimes
general_do = mysql -u root -e
dbdo = mysql -u root $(database_name) -e

demo: prepare_database remove_table prepare_table

	./insertPingtime.sh 10 1
	make plot_table

prepare_database:
	$(general_do) "create database if not exists $(database_name)"

prepare_table:
	$(dbdo) "create table if not exists $(table_name) (Zeitpunkt TIMESTAMP, URL VARCHAR(30), Ping FLOAT UNSIGNED)";

clean: remove_table remove_database

remove_database:
	$(general_do) "drop database $(database_name)"

remove_table:
	$(dbdo) "drop table $(table_name)"



insert_infinite_datapoints:
	watch -n 10 ./insertPingtime.sh

show_table:
	$(dbdo) "select * from $(table_name)"

plot_table:
	gnuplot graph.plot

show_plot:
	feh --reload 1 ping.png &
