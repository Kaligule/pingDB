database_name="pingDB"
table_name="pingtimes"
url="www.schauderbasis.de"
dbdo="mysql -u root -s $database_name -e"
# number of datapoints to generate: take first argument or 10 as
# default
n=${1:-10}
# sleeptime; take second argument or 1 as default
t=${2:-1}



for i in `seq $n`
do
    pingtime=$(ping -c 1 $url | sed -n 2p | awk '{print $8}' | cut -c 6-)
    $dbdo "insert into $table_name (Zeitpunkt, URL, Ping) values (NOW(), '$url', $pingtime);"
    sleep $t
done
         

